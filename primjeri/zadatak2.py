f = open('ulaz.txt')
sadrzaj_fajla = f.read()
linije = sadrzaj_fajla.split('\n')

broj_dana = int(linije[0])

temperature = linije[1:]

zbroj = 0
for temperatura in temperature:
    zbroj = zbroj + int(temperatura)

print(zbroj / broj_dana)
