import turtle

kornjaca = turtle.Turtle()

for i in range(4):
    kornjaca.color('blue')
    kornjaca.forward(50)
    kornjaca.left(360/8)

    kornjaca.color('red')
    kornjaca.forward(50)
    kornjaca.left(360/8)
