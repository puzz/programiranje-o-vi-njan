import random

broj_za_pogadjanje = random.randint(0, 100)

broj = input('Pogodi broj:')
while int(broj) != broj_za_pogadjanje:
    if int(broj) < broj_za_pogadjanje:
        print('Broj je veci')
    if int(broj) > broj_za_pogadjanje:
        print('Broj je manji')
    broj = input('Pogodi broj:')

print('Pogodili ste')
