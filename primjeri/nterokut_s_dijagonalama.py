import turtle

def nterokut_s_dijagonalama(broj_stranica):
    t = turtle.Turtle()
    pocetna_pozicija = t.pos()
    for i in range(broj_stranica):
        t.forward(100)
        t.left(360.0/broj_stranica)
        trenutna_pozicija = t.pos()
        t.setpos(pocetna_pozicija)
        t.setpos(trenutna_pozicija)

nterokut_s_dijagonalama(13)
